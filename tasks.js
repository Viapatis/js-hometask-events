'use strict';

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
 * @param {Event} event 
 */
function openMenu(event) {
  const open = !(event.target.dataset.open === 'true');
  event.target.dataset.open = open;
  document.getElementById('menu').classList.toggle('menu-opened',open);
}

/**
 * 
 * @param {Event} event 
 */
function closeMenu(event) {
  if (event.target.classList.contains('menu-item')) {
    document.getElementById('menuBtn').dataset.open = 'false';
    document.getElementById('menu').classList.remove('menu-opened');
  }
}
document.getElementById('menuBtn').addEventListener('click', openMenu);
document.getElementById('menu').addEventListener('click', closeMenu);

/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
 * @param {Event} event 
 */
function moveBlock(event) {
  const { clientX, clientY } = event;
  const { top, left, width, height } = event.currentTarget.getBoundingClientRect();
  const movedBlockElem = document.getElementById('movedBlock');
  const { clientWidth, clientHeight } = movedBlockElem;
  const y = clientY - top - clientHeight / 2,
    x = clientX - left - clientWidth / 2,
    reducedHeight = height - clientHeight,
    reducedWidth = width - clientWidth;
  movedBlockElem.style.top = (y < reducedHeight ? (y > 0 ? y : 0) : reducedHeight) + 'px';
  movedBlockElem.style.left = (x < reducedWidth ? (x > 0 ? x : 0) : reducedWidth) + 'px';
}
document.getElementById('field').addEventListener('click', moveBlock);
/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
 * @param {Event} event 
 */
function hideMessage(event) {
  if (event.target.classList.contains('remove')) {
    event.currentTarget.children[+event.target.dataset.id - 1].style.display = "none";
  }
}
document.getElementById('messager').addEventListener('click', hideMessage);

/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
*/
function questUser(event) {
  if (!confirm(`Вы точно хотите перейти по ссылке ${event.target.textContent}?`)) {
    event.preventDefault()
  }
}
Array.prototype.forEach.call(document.getElementsByClassName('link'), (link => {
  console.log(link);
  link.addEventListener('click', questUser);
}))
/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
*/
function changeHeader(event) {
  document.getElementById('taskHeader').textContent = event.target.value;

}
document.getElementById('fieldHeader').addEventListener('input', changeHeader);
